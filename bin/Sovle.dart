import 'Question.dart';

class Sovle {
  var allSolution = [];
  static var ans;
  List<List<String>> table1 = [
    ["2", "1", "4", "3"],
    ["4", "3", "2", "1"],
    ["3", "2", "1", "4"],
    ["1", "4", "3", "2"]
  ];
  List<List<String>> table2 = [
    ["1", "2", "3", "4"],
    ["4", "3", "1", "2"],
    ["2", "1", "4", "3"],
    ["3", "4", "2", "1"]
  ];
  List<List<String>> table3 = [
    ["4", "2", "3", "1"],
    ["1", "3", "4", "2"],
    ["3", "1", "2", "4"],
    ["2", "4", "1", "3"]
  ];
  List<List<String>> table4 = [
    ["1", "2", "4", "3"],
    ["4", "3", "1", "2"],
    ["2", "1", "3", "4"],
    ["3", "4", "2", "1"]
  ];
  List<List<String>> table5 = [
    ["4", "2", "1", "3"],
    ["1", "3", "2", "4"],
    ["3", "1", "4", "2"],
    ["2", "4", "3", "1"]
  ];
  List<List<String>> table6 = [
    ["3", "2", "4", "1"],
    ["4", "1", "3", "2"],
    ["2", "3", "1", "4"],
    ["1", "4", "2", "3"]
  ];

  void addTableToList() {
    allSolution.add(table1);
    allSolution.add(table2);
    allSolution.add(table3);
    allSolution.add(table4);
    allSolution.add(table5);
    allSolution.add(table6);
  }

  void setSovle(int index) {
    addTableToList();
    ans = allSolution.elementAt(index);
  }

  void getSovle() {
    print(ans);
  }
}
