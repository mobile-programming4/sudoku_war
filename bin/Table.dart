import 'dart:io';

import 'Game.dart';
import 'Player.dart';
import 'Question.dart';
import 'Sovle.dart';

class Table {
  bool miss = false;
  bool win = false;

  void updateTable() {
    for (int r = 0; r < Question.board.length; r++) {
      for (int c = 0; c < 4; c++) {
        stdout.write("${Question.board[r][c]} ");
      }
      print("");
    }
    if (win == false) {
      print("================================");
    }
  }

  bool setRowCol(int row, int col, String answer) {
    if ((row | col > 3) & (row | col < 0)) {
      return false;
    }
    if ((answer != "1") & (answer != "2") & (answer != "3") & (answer != "4")) {
      return false;
    }
    if (checkAnswer(row, col, answer)) {
      Question.board[row][col] = answer;
      updateTable();
    }
    return true;
  }

  bool checkAnswer(int row, int col, String answer) {
    if (answer == Sovle.ans[row][col]) {
      miss = false;
      return true;
    } else {
      miss = true;
      return false;
    }
  }

  bool checkWin() {
    for (int r = 0; r < Question.board.length; r++) {
      for (int c = 0; c < 4; c++) {
        if (Question.board[r][c] == "0") {
          return false;
        }
      }
    }
    return true;
  }

  bool isWin() {
    return win;
  }

  bool isDamge() {
    return true;
  }
}
