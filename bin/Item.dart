import 'Player.dart';

class Item {
  var name;
  var damage;
  var price;
  var player = Player();

  Item() {}

  void showShop() {
    print("||=====SHOP=====||");
    print("1.Dagger | Damage: 5 | Price: 20.00");
    print("2.Sword | Damage: 8 | Price: 35.00");
    print("================================");
  }

  void sold(String index) {
    if (index == "1") {
      if (player.getMoney() >= 20.0) {
        player.setAD(25);
        player.setMoney(player.getMoney() - 20.00);
        print("You bought dagger!!");
        player.showStatus();
      } else {
        print("Not enough money.");
        return;
      }
    } else if (index == "2") {
      if (player.getMoney() >= 35.00) {
        player.setAD(28);
        player.setMoney(player.getMoney() - 35.00);
        print("You bought sword!!");
        player.showStatus();
      } else {
        print("Not enough money.");
        return;
      }
    } else {
      return;
    }
  }

  void setName(String name) {
    this.name = name;
  }

  String getName() {
    return name;
  }

  void setDamagePoint(int damage) {
    this.damage = damage;
  }

  int getDamagePoint() {
    return damage;
  }

  void setPrice(double price) {
    this.price = price;
  }

  double getPrice() {
    return price;
  }
}
