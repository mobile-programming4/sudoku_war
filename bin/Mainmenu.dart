import 'dart:math';

import 'Game.dart';
import 'Player.dart';
import 'Question.dart';

void main(List<String> args) {
  Question question = Question();
  Game game = Game();
  Player player = Player();
  game.into();
  player.showStatus();
  game.selectMonster();
  question.getQuestions();
  while (true) {
    game.inputRowCol();
    if (game.isFinish()) {
      if (game.getNext() == "N") {
        break;
      }
    }
  }
}
