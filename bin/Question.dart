import 'dart:io';
import 'dart:math';

import 'Sovle.dart';

class Question {
  var allQuestion = [];
  static var board;
  int index = 0;

  List<List<String>> table1 = [
    ["2", "1", "0", "0"],
    ["0", "3", "2", "1"],
    ["0", "0", "0", "4"],
    ["1", "0", "0", "0"]
  ];
  List<List<String>> table2 = [
    ["1", "0", "3", "4"],
    ["0", "3", "0", "0"],
    ["2", "0", "0", "0"],
    ["0", "0", "0", "1"]
  ];
  List<List<String>> table3 = [
    ["4", "2", "0", "0"],
    ["1", "0", "0", "0"],
    ["0", "1", "2", "0"],
    ["0", "0", "0", "3"]
  ];
  List<List<String>> table4 = [
    ["0", "0", "0", "3"],
    ["4", "0", "0", "0"],
    ["0", "1", "3", "0"],
    ["3", "0", "2", "0"]
  ];
  List<List<String>> table5 = [
    ["0", "2", "0", "0"],
    ["0", "0", "2", "0"],
    ["3", "0", "4", "0"],
    ["2", "0", "0", "1"]
  ];
  List<List<String>> table6 = [
    ["0", "0", "0", "1"],
    ["4", "0", "0", "2"],
    ["2", "0", "0", "4"],
    ["0", "0", "2", "3"]
  ];

  void addTableToList() {
    allQuestion.add(table1);
    allQuestion.add(table2);
    allQuestion.add(table3);
    allQuestion.add(table4);
    allQuestion.add(table5);
    allQuestion.add(table6);
  }

  int randomQuestion() {
    addTableToList();
    final random = new Random();
    var random_index = Random().nextInt(6);
    return random_index;
  }

  void getQuestions() {
    index = randomQuestion();
    board = allQuestion.elementAt(index);
    Sovle().setSovle(index);
    for (int r = 0; r < board.length; r++) {
      for (int c = 0; c < 4; c++) {
        stdout.write("${board[r][c]} ");
      }
      print("");
    }
  }
}
