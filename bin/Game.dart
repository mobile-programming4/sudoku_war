import 'dart:io';

import 'Item.dart';
import 'MonsterLV1.dart';
import 'MonsterLV2.dart';
import 'MonsterLV3.dart';
import 'Player.dart';
import 'Question.dart';
import 'Table.dart';

class Game {
  var table = Table();
  var player = Player();
  var item = Item();
  var win = false;
  var next = "non";
  Stopwatch time = new Stopwatch();
  var timeDamage;
  var monster1 = MonsterLV1();
  var monster2 = MonsterLV2();
  var monster3 = MonsterLV3();
  var select;
  var monHp, monMaxHp, monAd = 0, monArmor, monCritical, monHpregen;
  var playerMoney;

  void newBoard() {
    this.table = new Table();
    Question().getQuestions();
    inputRowCol();
  }

  void into(){
    print(r'''
 _______           ______   _______  _                                    _______  _______ 
(  ____ \|\     /|(  __  \ (  ___  )| \    /\|\     /|          |\     /|(  ___  )(  ____ )
| (    \/| )   ( || (  \  )| (   ) ||  \  / /| )   ( |          | )   ( || (   ) || (    )|
| (_____ | |   | || |   ) || |   | ||  (_/ / | |   | |          | | _ | || (___) || (____)|
(_____  )| |   | || |   | || |   | ||   _ (  | |   | |          | |( )| ||  ___  ||     __)
      ) || |   | || |   ) || |   | ||  ( \ \ | |   | |          | || || || (   ) || (\ (   
/\____) || (___) || (__/  )| (___) ||  /  \ \| (___) |          | () () || )   ( || ) \ \__
\_______)(_______)(______/ (_______)|_/    \/(_______)          (_______)|/     \||/   \__/
                                                                                                      
''');
  print("");
  print(r'''+======Rule======+
- Select a monster for battle.
If you win. You'll get money for buy new weapon
Else the number miss match. You'll get damage by Monster's attack damage.
Until your health point equal 0. It's Game Over!
============================================================================
☻ Hint: About the effect each monster
1. Poring (Easy)
  - It's normal. just have armor.
2. Little devil (Medium)
  - Be careful for critical !! 
    If you put wrong number. It has a chance to critical (By critical is damage x 2)
3. Mud Boss (Hard)
  - Every put wrong number. It'll regen health point from 0 to 3.
  ''');
  }

  void inputRowCol() {
    time.start();
    if (table.checkWin()) {
      table.win = true;
      return;
    }
    stdout.write("Enter row (1-4): ");
    int row = int.parse(stdin.readLineSync()!) - 1;
    stdout.write("Enter col (1-4): ");
    int col = int.parse(stdin.readLineSync()!) - 1;
    stdout.write("Enter Number (1-4): ");
    String answer = stdin.readLineSync()!;
    table.setRowCol(row, col, answer);
    if (table.miss == true) {
      gotDamage();
    }
  }

  bool getWin() {
    return win;
  }

  void setWin(bool win) {
    this.win = win;
  }

  bool isFinish() {
    time.stop();
    if (table.isWin()) {
      table.updateTable();
      print("-+End+-");
      timeDamage = time.elapsedMilliseconds ~/ 1000;
      print("Time: $timeDamage secords.");
      calDamage(timeDamage);
      battle();
      time.reset();
      return true;
    }
    return false;
  }

  int calDamage(int time) {
    timeDamage = (player.getAD() * 10) ~/ time;
    print("Damage: $timeDamage");
    print("================================");
    return timeDamage;
  }

  void battle() {
    if (monHp - timeDamage <= 0 && monArmor == 0) {
      playerMoney = monMaxHp * 0.8;
      print("Success!!");
      print("you got money: $playerMoney");
      player.setMoney(player.getMoney() + playerMoney);
      player.showStatus();
      buy();
      nextRound();
    } else {
      if (select == "1") {
        if (monArmor - timeDamage > 0) {
          monArmor = monArmor - timeDamage;
          monster1.setArmor(monArmor);
          monster1.showAvatar();
          monster1.showStatus();
          newBoard();
        } else if (monArmor - timeDamage < 0) {
          monArmor = monArmor - timeDamage;
          monHp = monHp + monArmor;
          monster1.setHP(monHp);
          monArmor = 0;
          monster1.setArmor(monArmor);
          monster1.showAvatar();
          monster1.showStatus();
          newBoard();
        } else {
          monHp = monHp - (timeDamage - monArmor);
          monster1.setHP(monHp);
          monArmor = 0;
          monster1.setArmor(monArmor);
          monster1.showAvatar();
          monster1.showStatus();
          newBoard();
        }
      } else if (select == "2") {
        if (monArmor - timeDamage > 0) {
          monArmor = monArmor - timeDamage;
          monster2.setArmor(monArmor);
          monster2.showAvatar();
          monster2.showStatus();
          newBoard();
        } else if (monArmor - timeDamage < 0) {
          monArmor = monArmor - timeDamage;
          monHp = monHp + monArmor;
          monster2.setHP(monHp);
          monArmor = 0;
          monster2.setArmor(monArmor);
          monster2.showAvatar();
          monster2.showStatus();
          newBoard();
        } else {
          monHp = monHp - (timeDamage - monArmor);
          monster2.setHP(monHp);
          monArmor = 0;
          monster2.setArmor(monArmor);
          monster2.showAvatar();
          monster2.showStatus();
          newBoard();
        }
      } else if (select == "3") {
        if (monArmor - timeDamage > 0) {
          monArmor = monArmor - timeDamage;
          monster3.setArmor(monArmor);
          monster3.showAvatar();
          monster3.showStatus();
          newBoard();
        } else if (monArmor - timeDamage < 0) {
          monArmor = monArmor - timeDamage;
          monHp = monHp + monArmor;
          monster3.setHP(monHp);
          monArmor = 0;
          monster3.setArmor(monArmor);
          monster3.showAvatar();
          monster3.showStatus();
          newBoard();
        } else {
          monHp = monHp - (timeDamage - monArmor);
          monster3.setHP(monHp);
          monArmor = 0;
          monster3.setArmor(monArmor);
          monster3.showAvatar();
          monster3.showStatus();
          newBoard();
        }
      }
    }
  }

  bool buy() {
    item.showShop();
    print("Do you want to buy item?");
    stdout.write("Press Y/N: ");
    String choice = stdin.readLineSync()!;
    choice = choice.toUpperCase();
    if (choice == "Y") {
      stdout.write("Enter number (E = exit): ");
      String index = stdin.readLineSync()!;
      item.sold(index);
      return true;
    } else {
      return false;
    }
  }

  String getNext() {
    return next;
  }

  bool selectMonster() {
    print("Choose a monster for fighting!");
    print("1.Poring(Easy) || 2.Little devil(Medium) || 3.Mud Boss(Hard)");
    stdout.write("Enter number: ");
    select = stdin.readLineSync()!;
    switch (select) {
      case "1":
        MonsterLV1 monster1 = new MonsterLV1();
        monster1.showAvatar();
        monster1.showStatus();
        monHp = monster1.getHP();
        monMaxHp = monster1.getMaxHp();
        monAd = monster1.getAD();
        monArmor = monster1.getArmor();
        break;
      case "2":
        MonsterLV2 monster2 = new MonsterLV2();
        monster2.showAvatar();
        monster2.showStatus();
        monHp = monster2.getHP();
        monMaxHp = monster2.getMaxHp();
        monAd = monster2.getAD();
        monArmor = monster2.getArmor();
        break;
      case "3":
        MonsterLV3 monster3 = new MonsterLV3();
        monster3.showAvatar();
        monster3.showStatus();
        monHp = monster3.getHP();
        monMaxHp = monster3.getMaxHp();
        monAd = monster3.getAD();
        monArmor = monster3.getArmor();
        break;
      default:
        return false;
    }
    return true;
  }

  void gotDamage() {
    if (select == "2") {
      monster2.randomAD();
      monAd = monster2.getAD();
    } else if (select == "3") {
      monster3.regenHP(monHp);
      monHp = monster3.getHP();
    }
    print("================================");
    if (player.getHP() - monAd > 0) {
      print("--MISS!--");
      player.setHP(player.getHP() - monAd);
      print("You got $monAd damage");
      print("HP: ${player.getHP()}/${player.getMaxHp()}");
      monster2.setAD(3);
      table.updateTable();
    } else {
      print("_____**GAME OVER!**_____");
      exit(0);
    }
  }

  void nextRound() {
    print("Do you want to continue?");
    print("Press Y/N");
    next = stdin.readLineSync()!;
    next = next.toUpperCase();
    if (next == "Y") {
      selectMonster();
      newBoard();
    }
  }
}
