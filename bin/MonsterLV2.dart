import 'dart:math';

import 'Monster.dart';

class MonsterLV2 implements Monster {
  @override
  var ad = 3;

  @override
  var hp = 10;

  @override
  var maxHp = 10;

  var armor = 5;

  var critical = 2;

  var arrayDamage = [
    3,
    6,
    3,
    6,
    3,
  ];

  @override
  int getAD() {
    return ad;
  }

  @override
  int getHP() {
    return hp;
  }

  @override
  int getMaxHp() {
    return maxHp;
  }

  int getArmor() {
    return armor;
  }

  int getCritical() {
    return critical;
  }

  void setCritical(int critical) {
    this.critical = critical;
  }

  @override
  void setAD(int ad) {
    this.ad = ad;
  }

  @override
  void setHP(int hp) {
    this.hp = hp;
  }

  @override
  void setMaxHp(int maxHp) {
    this.maxHp = maxHp;
  }

  void setArmor(int armor) {
    this.armor = armor;
  }

  @override
  void showAvatar() {
    print("");
    print("∋(▼ｰ▼Ψ｡)∈~→");
    print("");
  }

  @override
  void showStatus() {
    print("HP: $hp/$maxHp");
    print("Armor: $armor");
    print("AD: $ad");
    print("================================");
  }

  int randomAD() {
    var randomDamage = Random().nextInt(5);
    this.ad = (arrayDamage[randomDamage]);
    return ad;
  }
}
