class Player {
  var hp = 20;
  var maxHp = 20;
  var ad = 20;
  var money = 0.00;

  Player() {}

  void showStatus() {
    print("-+-+Player+-+-");
    print("HP: $hp/$maxHp");
    print("AD: $ad");
    print("Money: $money");
    print("-+-+-+--+-+-+-");
  }

  void setMaxHp(int maxHp) {
    this.maxHp = maxHp;
  }

  int getMaxHp() {
    return maxHp;
  }

  void setAD(int ad) {
    this.ad = ad;
  }

  int getAD() {
    return ad;
  }

  void setHP(int hp) {
    this.hp = hp;
  }

  int getHP() {
    return hp;
  }

  void setMoney(double money) {
    this.money = money;
  }

  double getMoney() {
    return money;
  }
}
