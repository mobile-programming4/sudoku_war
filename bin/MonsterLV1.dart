import 'Monster.dart';

class MonsterLV1 implements Monster {
  @override
  var ad = 2;

  @override
  var hp = 5;

  @override
  var maxHp = 5;

  var armor = 2;

  @override
  int getAD() {
    return ad;
  }

  @override
  int getHP() {
    return hp;
  }

  @override
  int getMaxHp() {
    return maxHp;
  }

  int getArmor() {
    return armor;
  }

  @override
  void setAD(int ad) {
    this.ad = ad;
  }

  @override
  void setHP(int hp) {
    this.hp = hp;
  }

  @override
  void setMaxHp(int maxHp) {
    this.maxHp = maxHp;
  }

  void setArmor(int armor) {
    this.armor = armor;
  }

  @override
  void showAvatar() {
    print("");
    print("ヘ(◕。◕ヘ)");
    print("");
  }

  @override
  void showStatus() {
    print("HP: $hp/$maxHp");
    print("Armor: $armor");
    print("AD: $ad");
    print("================================");
  }
}
