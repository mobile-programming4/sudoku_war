abstract class Monster {
  var hp;
  var ad;
  var maxHp;

  Monster() {}

  void showStatus() {
    print("HP: $hp/$maxHp");
    print("AD: $ad");
  }

  void showAvatar() {}

  void setMaxHp(int maxHp) {
    this.maxHp = maxHp;
  }

  int getMaxHp() {
    return maxHp;
  }

  void setAD(int ad) {
    this.ad = ad;
  }

  int getAD() {
    return ad;
  }

  void setHP(int hp) {
    this.hp = hp;
  }

  int getHP() {
    return hp;
  }
}
