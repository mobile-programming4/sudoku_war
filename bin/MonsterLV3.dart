import 'dart:math';

import 'Monster.dart';

class MonsterLV3 implements Monster {
  @override
  var ad = 10;

  @override
  var hp = 20;

  @override
  var maxHp = 20;

  var armor = 0;

  var hpRegen = 3;

  @override
  int getAD() {
    return ad;
  }

  @override
  int getHP() {
    return hp;
  }

  @override
  int getMaxHp() {
    return maxHp;
  }

  int getArmor() {
    return armor;
  }

  int getHpRegen() {
    return hpRegen;
  }

  @override
  void setAD(int ad) {
    this.ad = ad;
  }

  @override
  void setHP(int hp) {
    this.hp = hp;
  }

  @override
  void setMaxHp(int maxHp) {
    this.maxHp = maxHp;
  }

  void setHpRegen() {
    this.hpRegen = hpRegen;
  }

  void setArmor(int armor) {
    this.armor = armor;
  }

  @override
  void showAvatar() {
    print("");
    print("ლ༼ ▀̿ Ĺ̯ ▀̿ ლ ༽");
    print("");
  }

  @override
  void showStatus() {
    print("HP: $hp/$maxHp");
    print("Armor: $armor");
    print("AD: $ad");
    print("================================");
  }

  int regenHP(int currentHp) {
    var regenRandom = Random().nextInt(4);
    if (hp + currentHp < maxHp) {
      hp + currentHp;
    } else {
      hp = maxHp;
    }
    return hp;
  }
}
